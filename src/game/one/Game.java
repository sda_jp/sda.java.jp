package game.one;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = -5279751763070626964L;

	public static final int WIDTH = 640, HEIGHT = WIDTH / 12 * 9; // 640x480

	private Thread thread;
	private boolean running = false;

	private Handler handler;
	private HealthBarStatus hbs;
	private Random r;
	public Logic logic;

	public Game() throws IOException {
		handler = new Handler();
		this.addKeyListener(new KeyInput(handler));

		new Window(WIDTH, HEIGHT, "Pierwsza gra", this);

		hbs = new HealthBarStatus();
		r = new Random();
		logic = new Logic(handler, hbs);

		handler.addObject(new Enemy(WIDTH / 2 - 10, r.nextInt(HEIGHT / 2 - 10), ID.Enemy, handler));
		handler.addObject(new Player(WIDTH / 2, HEIGHT / 2, ID.Player, handler));

	}

	// starting the thread with a game
	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}

	// stopping the thread
	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Custom game loop for updating state of the game
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountofTicks = 60.0;
		double ns = 1000000000 / amountofTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running)
				render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				// System.out.println("FPS: " + frames);
				frames = 0;
			}
		}
		stop();
	}

	private void tick() {
		handler.tick();
		hbs.tick();
		logic.tick();
	}

	// Windowing updated window
	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		
		//g.setColor(Color.black);
		LoadImageApp imgBack = new LoadImageApp("backgroundImage3.jpg");
		imgBack.paint(g, 0,0);
			
		//g.fillRect(0, 0, WIDTH, HEIGHT);

		handler.render(g);

		hbs.render(g);

		g.dispose();
		bs.show();
	}

	public static int clamp(int var, int min, int max) {
		if (var >= max)
			return var = max;
		if (var <= min)
			return var = min;
		else
			return var;
	}

	public static void main(String[] args) throws IOException {
		new Game();
	}

}
