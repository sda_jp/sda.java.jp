package game.one;

public enum ID {
	Player(),
	Enemy(),
	GameOver(), 
	Bullet(),
	Monster(), 
	Life();
}
