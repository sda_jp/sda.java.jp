package game.one;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class KeyInput extends KeyAdapter {
	private Handler handler;
	private Random r = new Random();
	public static boolean bulletFlag = false;

	public KeyInput(Handler handler) {
		this.handler = handler;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		for (int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			

			if (tempObject.getId() == ID.Player) {
				// key events for player
				if (key == KeyEvent.VK_UP)
					tempObject.setVelY(-3);
				if (key == KeyEvent.VK_DOWN)
					tempObject.setVelY(3);
				if (key == KeyEvent.VK_RIGHT)
					tempObject.setVelX(3);
				if (key == KeyEvent.VK_LEFT)
					tempObject.setVelX(-3);
			}
		}

		for (int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			if (tempObject.getId() == ID.Player) {
				// key events for GENERATING BULLETS
				
				if (key == KeyEvent.VK_W && !bulletFlag) {
					Bullet bullet = new Bullet(tempObject.getX() + 25, tempObject.getY(), ID.Bullet, handler);
					handler.addObject(bullet);
					bullet.setVelY(-5);
					bullet.setVelX(r.nextInt((7 - 2) + 1) - 2);
					bulletFlag = true;
				}
				if (key == KeyEvent.VK_S && !bulletFlag) {
					Bullet bullet = new Bullet(tempObject.getX() + 25, tempObject.getY(), ID.Bullet, handler);
					handler.addObject(bullet);
					bullet.setVelY(5);
					bullet.setVelX(r.nextInt((7 - 2) + 1) - 2);
					bulletFlag = true;
				}
				if (key == KeyEvent.VK_A && !bulletFlag) {
					Bullet bullet = new Bullet(tempObject.getX() + 25, tempObject.getY(), ID.Bullet, handler);
					handler.addObject(bullet);
					bullet.setVelX(-5);
					bullet.setVelY(r.nextInt((7 - 2) + 1) - 2);
					bulletFlag = true;
				}
				if (key == KeyEvent.VK_D && !bulletFlag) {
					Bullet bullet = new Bullet(tempObject.getX() + 25, tempObject.getY(), ID.Bullet, handler);
					handler.addObject(bullet);
					bullet.setVelX(5);
					bullet.setVelY(r.nextInt((7 - 2) + 1) - 2);
					bulletFlag = true;
				}
			}
		}

		if (key == KeyEvent.VK_ESCAPE)
			System.exit(1);
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		for (int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);

			if (tempObject.getId() == ID.Player) {
				if (key == KeyEvent.VK_UP)
					tempObject.setVelY(0);
				if (key == KeyEvent.VK_DOWN)
					tempObject.setVelY(0);
				if (key == KeyEvent.VK_RIGHT)
					tempObject.setVelX(0);
				if (key == KeyEvent.VK_LEFT)
					tempObject.setVelX(0);
			}
		}
	}
}
