package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Monster extends GameObject{
	
	Random r = new Random();
	Handler handler;
	public Monster(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;	
		velX = 5;
		velY = 5;		
	}
	public Rectangle getBounds(){
		return new Rectangle(x, y, 40, 46);
	}

	public void tick() {

		x += velX;
		y += velY;
		
		if (x < 0 || x > 590) {
			velX = -velX;
		} 
		if (y < 0 || y > 420){
			velY = -velY;
		}
//		collisionMonster();
	}

	public void render(Graphics g) {
		LoadImageApp imgMonster = new LoadImageApp("monster.png");
		imgMonster.paint(g, x, y);

	}
	private void collisionMonster() {
		for(int i = 0; i < handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			if (tempObject.getId() == ID.Enemy){
				if(getBounds().intersects(tempObject.getBounds())){

				}
				
			}
			
		}
		
	}
}
