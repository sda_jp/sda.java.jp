package game.one;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class HealthBarStatus {

	public static int HEALTH = 100;

	private int greenValue = 255;

	private int score;
	private int level = 1;
	private int live;

	public void tick() {
		HEALTH = Game.clamp(HEALTH, 0, 100);
		greenValue = Game.clamp(greenValue, 0, 255);
		greenValue = HEALTH * 2;
	}

	public void render(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(15, 15, 200, 10);
		g.setColor(new Color(175, greenValue, 0));
		g.fillRect(15, 15, 2 * HEALTH, 10);
		g.setColor(Color.WHITE);
		g.drawRect(15, 15, 200, 10);

		Font font = new Font("Bauhaus 93", Font.PLAIN, 20); // "Stencil"
		g.setFont(font);

		g.drawString("SCORE: " + score, 15, 45);
		g.drawString("LEVEL: " + level, 15, 65);
		g.drawString("LIVE: " + live, 550, 20);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLive() {
		return live;
	}

	public void setLive(int d) {
		this.live = d;
	}

}
