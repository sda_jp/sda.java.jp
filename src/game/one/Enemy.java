package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Enemy extends GameObject{
	
	Random r = new Random();
	Handler handler;

	public Enemy(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		velX = r.nextInt((4 - 2) + 1) + 2;
		velY = r.nextInt((4 - 2) + 1) + 2;
	}
	public Rectangle getBounds(){
		return new Rectangle(x, y, 50, 43);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		if (x < 0 || x > 590) {
			velX = -velX;
		} 
		if (y < 0 || y > 420){
			velY = -velY;
		}
		collisionEnemy();
	}

	public void render(Graphics g) {
		LoadImageApp imgEnemy = new LoadImageApp("enemy.png");
		imgEnemy.paint(g, x, y);

	}
	private void collisionEnemy() {
		for(int i = 0; i < handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			if (tempObject.getId() == ID.Enemy){
				if(getBounds().intersects(tempObject.getBounds())){
//					tempObject.setVelX( tempObject.getVelX() * (-1)); 
//					tempObject.setVelY( tempObject.getVelY() * (-1)); 
				}
				
			}
			
		}
		
	}
}
