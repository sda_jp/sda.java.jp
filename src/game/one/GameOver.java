package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;

public class GameOver extends GameObject {
	private int x, y;
	private int velY;

	public GameOver(int x, int y, int velY, ID id) {
		super(velY, velY, id);
		this.x = x;
		this.y = y;
		this.velY = velY;
	}

	public void tick() {
		y += velY;
		if (y == 210) {
			velY = 0;
		}
	}

	public void render(Graphics g) {
		LoadImageApp imgGameOver = new LoadImageApp("GameOver.png");
		imgGameOver.paint(g, x, y);
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}
}
