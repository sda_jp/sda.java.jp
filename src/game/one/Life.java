package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Life extends GameObject{
	Random r = new Random();
	Handler handler;
	public static boolean lifeFlag = false;

	public Life(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		velX = 0;
		velY = 7;
	}
	public Rectangle getBounds(){
		return new Rectangle(x, y, 31, 27);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		collisionLife();
	}

	public void render(Graphics g) {
		LoadImageApp imgLife = new LoadImageApp("life.png");
		imgLife.paint(g, x, y);

	}
	private void collisionLife() {
		for(int i = 0; i < handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			if (tempObject.getId() == ID.Player){
				if(getBounds().intersects(tempObject.getBounds())){
					lifeFlag = true;
					handler.removeObject(this);
				}
				
			}
			
		}
		
	}
}
