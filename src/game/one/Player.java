package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Player extends GameObject {
	
	Random r = new Random();
	Handler handler;
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, 49, 49);
	}

	public void tick() {
		x += velX;
		y += velY;
		if (x < 0 || x > 590) {
			velX = -velX;
		} 
		if (y < 0 || y > 420){
			velY = -velY;
		}
		
		collision();
		
	}

	private void collision() {
		for (int i = 0; i < handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			if(tempObject.getId() == ID.Enemy){
				if(getBounds().intersects(tempObject.getBounds())){
					HealthBarStatus.HEALTH -= 2;
				}
			}
		}
		
	}

	public void render(Graphics g) {
		LoadImageApp imgPlayer = new LoadImageApp("robot.png");
		imgPlayer.paint(g, x, y);

	}
}
